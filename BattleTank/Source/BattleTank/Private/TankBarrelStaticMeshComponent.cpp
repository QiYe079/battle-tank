// Fill out your copyright notice in the Description page of Project Settings.

#include "TankBarrelStaticMeshComponent.h"
#include "Tank.h"


UTankBarrelStaticMeshComponent::UTankBarrelStaticMeshComponent()
	:BarrelElevateSpeed(0.2f)
	,BarrelMinPitch(-5.f), BarrelMaxPitch(80.f)
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UTankBarrelStaticMeshComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get the owner of this component
	Owner = Cast<ATank>(GetOwner());
}

void UTankBarrelStaticMeshComponent::Elevate(float LaunchPitch)
{
	// First get the current pitch of this barrel
	float BarrelPitch = GetComponentRotation().Pitch;

	// Get the delta pitch to apply for this frame
	float DeltaPitch = LaunchPitch - BarrelPitch;
	if (DeltaPitch > 0)
	{
		DeltaPitch = FMath::Min(DeltaPitch, BarrelElevateSpeed);
	}
	else
	{
		DeltaPitch = FMath::Abs(DeltaPitch) < BarrelElevateSpeed ? DeltaPitch : -BarrelElevateSpeed;
	}

	// Add delta pitch to Barrel if it's within pitch range
	if (!(BarrelPitch >= BarrelMaxPitch && DeltaPitch >= 0) &&
		!(BarrelPitch <= BarrelMinPitch && DeltaPitch <= 0))
	{
		AddLocalRotation(FRotator(DeltaPitch, 0.f, 0.f));
	}
}
