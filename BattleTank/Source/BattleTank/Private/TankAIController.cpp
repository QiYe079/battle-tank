// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "Tank.h"

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();

	// Log to console
	LogMessageToConsole();

	// Get controlled and player's tank
	ControlledTank = Cast<ATank>(GetPawn());
	PlayerTank = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());

	UE_LOG(LogTemp, Error, TEXT("%s reporting: player tank: %s"), *GetName(), *PlayerTank->GetName());
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Rotate AI to face the player

	// Aim at the player
	ControlledTank->AimAt(PlayerTank->GetActorLocation());

	// Fire!
	ControlledTank->Fire();
}

void ATankAIController::LogMessageToConsole() const
{
	if (ControlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("AI Tank: %s has begun playing!"), *ControlledTank->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No AI associated with this controller! "));
	}

	if (PlayerTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Tank: %s has begun playing!"), *PlayerTank->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No Player Tank found! "));
	}
}

