// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrackStaticMeshComponent.h"
#include "Tank.h"




void UTankTrackStaticMeshComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get the owner of this component
	Owner = Cast<ATank>(GetOwner());
}


void UTankTrackStaticMeshComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	// UE_LOG(LogTemp, Warning, TEXT("Track component ticking."));

	// Associate the move function
	if (Owner->IsForwarding())
	{
		ForwardThrottle(Owner->GetForceToMove());
	}
	else if (Owner->IsReversing())
	{
		ReverseThrottle(Owner->GetForceToMove());
	}

}


void UTankTrackStaticMeshComponent::ForwardThrottle(float Force)
{
	UE_LOG(LogTemp, Warning, TEXT("Track forwarding with force: %f"), Force);

	UStaticMeshComponent* RootComp = Cast<UStaticMeshComponent>(Owner->GetRootComponent());
	if (RootComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Forwaring!!"));
		RootComp->AddForceAtLocation(FVector::ForwardVector * Force, GetComponentLocation());
	}

}


void UTankTrackStaticMeshComponent::ReverseThrottle(float Force)
{
	UE_LOG(LogTemp, Warning, TEXT("Track reversing with force: %f"), Force);

	UStaticMeshComponent* RootComp = Cast<UStaticMeshComponent>(Owner->GetRootComponent());
	if (RootComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("Reversing!!"));
		RootComp->AddForceAtLocation(FVector::ForwardVector * -Force, GetComponentLocation());
	}
}