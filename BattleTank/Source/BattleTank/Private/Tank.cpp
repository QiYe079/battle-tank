// Fill out your copyright notice in the Description page of Project Settings.

#include "Tank.h"
#include "TankAimComponent.h"
#include "TankBarrelStaticMeshComponent.h"
#include "TankTurretStaticMeshComponent.h"
#include "TankProjectileMovementComponent.h"
#include "TankProjectile.h"
#include "TankTrackStaticMeshComponent.h"

// Sets default values
ATank::ATank()
	:LaunchSpeed(5000.f)
	, ReloadTimeInSeconds(5.f)
	, LastTimeFired(-ReloadTimeInSeconds)
	, ForceToMove(56000 * 2.6)   // Make acceleration 2.6 m^2/s
	, bIsForwarding(false)
	, bisReversing(false)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create components
	TankAimComp = CreateDefaultSubobject<UTankAimComponent>(TEXT("TankAimComponent"));

}

// Called when the game starts or when spawned
void ATank::BeginPlay()
{
	// Call to base class, which redirects the BeginPlay in BP
	Super::BeginPlay();
	
}

// Called every frame
void ATank::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Tick track component
	LeftTrackComp->TickComponent(DeltaTime, ELevelTick::LEVELTICK_All, nullptr);
	RightTrackComp->TickComponent(DeltaTime, ELevelTick::LEVELTICK_All, nullptr);

}

// Called to bind functionality to input
void ATank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


void ATank::AimAt(const FVector& AimLocation)
{
	if (!TankAimComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No Tank Aim Component!! Please check BP."));
		return;
	}
	
	// Delegate to Aim Component to handle aiming
	TankAimComp->AimAt(AimLocation, LaunchSpeed);
}


void ATank::Fire()
{
	// UE_LOG(LogTemp, Warning, TEXT("Fired!!"));

	if (!TankAimComp)
	{
		UE_LOG(LogTemp, Warning, TEXT("No Tank Aim Component for firing!! Please check BP."));
		return;
	}

	// If still reloading, do nothing
	if ((GetWorld()->TimeSeconds - LastTimeFired) < ReloadTimeInSeconds)
		return;

	// UE_LOG(LogTemp, Warning, TEXT("Fired!!"));
	// Specify spawn parameters
	FActorSpawnParameters ProjectileSpawnParams;
	ProjectileSpawnParams.Owner = this;
	ProjectileSpawnParams.Instigator = Instigator;
	ProjectileSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	// Spawn the actor at the projectile socket of barrel
	ATankProjectile* SpawnedProjectile = GetWorld()->SpawnActor<ATankProjectile>(
		ProjectileClass,
		BarrelComp->GetSocketLocation(FName("Projectile")),
		BarrelComp->GetSocketRotation(FName("Projectile")),
		ProjectileSpawnParams
		);

	if (SpawnedProjectile)
	{
		UE_LOG(LogTemp, Warning, TEXT("Spawned Projectile location: %s"), *SpawnedProjectile->GetActorLocation().ToString());

		// Fire the projectile in the direction
		SpawnedProjectile->Launch(LaunchSpeed);

		// Update the firing time
		LastTimeFired = GetWorld()->TimeSeconds;
	}

	

}


void ATank::SetComponentsReference(UTankBarrelStaticMeshComponent* Barrel, UTankTurretStaticMeshComponent* Turret, 
	UTankTrackStaticMeshComponent* LeftTrack, UTankTrackStaticMeshComponent* RightTrack)
{
	if (!TankAimComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No Tank Aim Component!! Please check BP."));
		return;
	}

	// Set owning barrel and turret components
	BarrelComp = Barrel;
	TurretComp = Turret;
	LeftTrackComp = LeftTrack;
	RightTrackComp = RightTrack;

	// Delegate to Aim Component to set Barrel reference
	TankAimComp->SetBarrelStaticComponent(Barrel);
	TankAimComp->SetTurretStaticComponent(Turret);
}

bool ATank::IsForwarding() const
{
	return bIsForwarding;
}


bool ATank::IsReversing() const
{
	return bisReversing;
}


void ATank::SetForwarding(bool IsForwarding)
{
	//UE_LOG(LogTemp, Warning, TEXT("Forward set."));
	bIsForwarding = IsForwarding;
}


void ATank::SetReversing(bool IsReversing)
{
	//UE_LOG(LogTemp, Warning, TEXT("Reverse set."));
	bisReversing = IsReversing;
}


float ATank::GetForceToMove() const
{
	return ForceToMove;
}

