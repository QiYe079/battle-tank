// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurretStaticMeshComponent.h"
#include "Tank.h"



UTankTurretStaticMeshComponent::UTankTurretStaticMeshComponent()
	:RotateSpeed(0.2f)
{
	PrimaryComponentTick.bCanEverTick = false;


}


void UTankTurretStaticMeshComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get its owner
	Owner = Cast<ATank>(GetOwner());
}


void UTankTurretStaticMeshComponent::Rotate(float LaunchYaw)
{
	// First get the current pitch of this barrel
	float TurretYaw = GetComponentRotation().Yaw;

	// Get the delta pitch to apply for this frame
	float DeltaYaw = LaunchYaw - TurretYaw;
	if (DeltaYaw > 0)
	{
		DeltaYaw = FMath::Min(DeltaYaw, RotateSpeed);
	}
	else
	{
		DeltaYaw = FMath::Abs(DeltaYaw) < RotateSpeed ? DeltaYaw : -RotateSpeed;
	}

	// Add delta pitch to Barrel if it's within pitch range
	AddLocalRotation(FRotator(0, DeltaYaw, 0.f));
}
