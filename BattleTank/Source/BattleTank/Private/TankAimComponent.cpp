// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimComponent.h"
#include "TankBarrelStaticMeshComponent.h"
#include "TankTurretStaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Tank.h"


// Sets default values for this component's properties
UTankAimComponent::UTankAimComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UTankAimComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	Owner = Cast<ATank>(GetOwner());
}


void UTankAimComponent::AimAt(const FVector& TargetLocation, float LaunchSpeed)
{
	// UE_LOG(LogTemp, Warning, TEXT("%s is aiming Location: %s with launch speed: %f"), *Owner->GetName(), *TargetLocation.ToString(), LaunchSpeed);

	TArray<AActor*> IgnoredActors{ Owner }; // Add its owner to ignore
	if (UGameplayStatics::SuggestProjectileVelocity(
		this,														// World context object
		WorldAimDirection,											// (Output) Toss Velocity
		BarrelStaticComp->GetSocketLocation(FName("Projectile")),	// Start location: Projectile socket on Barrel
		TargetLocation,												// End location
		LaunchSpeed,												// Launch speed
		false, 0.f, 0.f,											// Low arc, collision radius 0, not overriding gravity
		ESuggestProjVelocityTraceOption::DoNotTrace,				// Do not trace
		FCollisionResponseParams::DefaultResponseParam,				// Default response parameters
		IgnoredActors,												// Ignored actors	
		true))														// Draw debug lines
	{
		// Get the unit vector of Toss Velocity
		WorldAimDirection.Normalize();

		//UE_LOG(LogTemp, Warning, TEXT("Suggested launch velocity: %s"), *TossVelocity.ToString());

		// Move the rotation of the barrel (pitch only) and turret (yaw only)
		BarrelStaticComp->Elevate(WorldAimDirection.Rotation().Pitch);
		TurretStaticComp->Rotate(WorldAimDirection.Rotation().Yaw);
	}
}


void UTankAimComponent::SetBarrelStaticComponent(UTankBarrelStaticMeshComponent* Barrel)
{
	BarrelStaticComp = Barrel;
	UE_LOG(LogTemp, Warning, TEXT("Barrel Location: %s"), *BarrelStaticComp->GetComponentLocation().ToString());
}


void UTankAimComponent::SetTurretStaticComponent(UTankTurretStaticMeshComponent* Turret)
{
	TurretStaticComp = Turret;
	UE_LOG(LogTemp, Warning, TEXT("TUrret Location: %s"), *TurretStaticComp->GetComponentLocation().ToString());
}


FVector UTankAimComponent::GetWorldAimDirection() const
{
	// Make sure the returned vector is normalized
	return WorldAimDirection.GetSafeNormal();
}

// Called every frame
void UTankAimComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Move barrel every frame

}

