// Fill out your copyright notice in the Description page of Project Settings.

#include "TankProjectile.h"
#include "TankProjectileMovementComponent.h"


// Sets default values
ATankProjectile::ATankProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// Create default object of projectile movement component
	ProjectileMoveComp = CreateDefaultSubobject<UTankProjectileMovementComponent>(TEXT("ProjectileMoveComp"));
	ProjectileMoveComp->bAutoActivate = false; // No activation at construction; only activate when launching projectile.
}

// Called when the game starts or when spawned
void ATankProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}


void ATankProjectile::Launch(float LaunchSpeed)
{
	if (ProjectileMoveComp)
	{
		ProjectileMoveComp->SetVelocityInLocalSpace(FVector::ForwardVector * LaunchSpeed);
		ProjectileMoveComp->Activate();
	}

}


