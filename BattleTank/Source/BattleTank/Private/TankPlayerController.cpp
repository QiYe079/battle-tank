// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Tank.h"
#include "DrawDebugHelpers.h"



ATankPlayerController::ATankPlayerController()
	:LineTraceDistance(100000.f)
	,HorizontalSensitivity(100.f)
	,VerticalSensitivity(80.f)
{
	PrimaryActorTick.bCanEverTick = true;


}


void ATankPlayerController::BeginPlay()
{
	// Make sure the Blueprint BeginPlay is called after C++
	Super::BeginPlay();

	// Log to console
	LogMessageToConsole();

	// Get controlled tank
	ControlledTank = Cast<ATank>(GetPawn());
}


void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// UE_LOG(LogTemp, Warning, TEXT("%s: ticking"), *GetName());

	// Keep aiming towards the crosshair
	AimTowardsCrosshair();
}


void ATankPlayerController::LogMessageToConsole() const
{
	if (ControlledTank)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player Tank: %s has begun playing!"), *ControlledTank->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No Player Tank associated with this player controller."));
	}
}


void ATankPlayerController::AimTowardsCrosshair()
{
	if (!ControlledTank) return;

	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation))
	{
		// Tell Tank to aim at the hit location
		ControlledTank->AimAt(HitLocation);
	}
}


bool ATankPlayerController::GetSightRayHitLocation(FVector& OutHitLocation)
{
	// Find the crosshair position on the screen
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D CrosshairViewportLocation(ViewportSizeX * 0.5f, ViewportSizeY * 0.5f);

	// "De-project" it to the world direction. The World location and Direction are
	// based on Viewport size, not resolution!!!
	FVector CrosshairWorldLocation, CrosshairWorldDirection;
	if (!DeprojectScreenPositionToWorld(CrosshairViewportLocation.X, CrosshairViewportLocation.Y, CrosshairWorldLocation, CrosshairWorldDirection))
		return false;

	// Get trace line end
	FVector TraceEnd = CrosshairWorldLocation + CrosshairWorldDirection * LineTraceDistance;
	// DrawDebugLine(GetWorld(), CrosshairWorldLocation, TraceEnd, FColor::Red, false, -1.f, 0, 5.f);

	// Perform single channel complex line tracing
	return SingleChannelLineTracingComplex(OutHitLocation, CrosshairWorldLocation, TraceEnd);

}


bool ATankPlayerController::SingleChannelLineTracingComplex(FVector &OutHitLocation, const FVector& TraceStart, const FVector& TraceEnd, ECollisionChannel ChannelType, bool bComplex)
{
	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(GetPawn());	// Ignore the controlled pawn
	CollisionQueryParams.bTraceComplex = bComplex;		// Perform complex tracing

	// Line trace through that direction to check if hit
	FHitResult HitResult;
	bool IsHit = GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ChannelType, CollisionQueryParams);
	if (IsHit)
	{
		OutHitLocation = HitResult.ImpactPoint;
	}

	return IsHit;
}
