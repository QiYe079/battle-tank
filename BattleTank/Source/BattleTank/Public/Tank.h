// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h"

class UTankAimComponent;
class UTankBarrelStaticMeshComponent;
class UTankTurretStaticMeshComponent;
class UTankProjectileMovementComponent;
class ATankProjectile;
class UTankTrackStaticMeshComponent;

UCLASS()
class BATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATank();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The aim component to handle the aiming
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UTankAimComponent* TankAimComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Properties")
	TSubclassOf<ATankProjectile> ProjectileClass;

	// The launch speed of projectiles of this tank
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float LaunchSpeed;

	// The time needed for reloading
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float ReloadTimeInSeconds;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Properties)
	float ForceToMove;

	// Keep track of the last time to fire the weapon
	float LastTimeFired;

	// Keep track of barrel, turret and tracks components
	UTankBarrelStaticMeshComponent* BarrelComp;
	UTankTurretStaticMeshComponent* TurretComp;
	UTankTrackStaticMeshComponent* LeftTrackComp;
	UTankTrackStaticMeshComponent* RightTrackComp;

	// keep track of forwarding or moving
	bool bIsForwarding;
	bool bisReversing;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Tell the tank to aim at target location
	UFUNCTION(BlueprintCallable)
	void AimAt(const FVector& AimLocation);

	// Input function to fire
	UFUNCTION(BlueprintCallable)
	void Fire();
	
	// Set the barrel and turret pointer to Aim Component to keep track of them.
	// To be called in BP because they are created in BP.
	UFUNCTION(BlueprintCallable)
	void SetComponentsReference(UTankBarrelStaticMeshComponent* Barrel, 
		UTankTurretStaticMeshComponent* Turret, UTankTrackStaticMeshComponent* LeftTrack, UTankTrackStaticMeshComponent* RightTrack);

	// Interface to check if the tank is forwarding or reversing, or set their values;
	bool IsForwarding() const;
	bool IsReversing() const;

	UFUNCTION(BlueprintCallable)
	void SetForwarding(bool IsForwarding);

	UFUNCTION(BlueprintCallable)
	void SetReversing(bool IsReversing);

	// Get the force to move the tank
	float GetForceToMove() const;
};
