// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrackStaticMeshComponent.generated.h"

class ATank;

/**
 * 
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrackStaticMeshComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UTankTrackStaticMeshComponent() = default;

protected:
	virtual void BeginPlay() override;

	// The owner of this component
	ATank* Owner;
	
public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	
	// Apply a forward force to Tank
	void ForwardThrottle(float Force);
	
	// Appy the reverse force to Tank
	void ReverseThrottle(float Force);
};
