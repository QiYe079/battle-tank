// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class ATank;

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATankPlayerController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly)
	ATank* ControlledTank;

	void LogMessageToConsole() const;

	// Keeps aiming towards the crosshair
	UFUNCTION(BlueprintCallable)
	void AimTowardsCrosshair();

	// The function to "de-project" the crosshair location on the 
	// screen to the world vector, and do line tracing to get the hit location.
	// NOTE: this method is used when the crosshair is not at the middle
	// of screen; it's in the middle, just use camera's viewpoint.
	UFUNCTION(BlueprintCallable)
	bool GetSightRayHitLocation(FVector& OutHitLocation);

	// Perform single channel complex line tracing
	bool SingleChannelLineTracingComplex(FVector &OutHitLocation, const FVector& TraceStart, const FVector& TraceEnd, ECollisionChannel ChannelType = ECollisionChannel::ECC_Visibility, bool bComplex = true);

	// The max distance to perform line tracing
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Crosshair")
	float LineTraceDistance;

	// The moving horizontal sensitivity
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Crosshair")
	float HorizontalSensitivity;

	// The moving vertical sensitivity
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Crosshair")
	float VerticalSensitivity;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

};
