// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TankProjectile.generated.h"

class UTankProjectileMovementComponent;

UCLASS()
class BATTLETANK_API ATankProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATankProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The projectile movement component to handle the moving of this projectile
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UTankProjectileMovementComponent* ProjectileMoveComp;

public:	
	void Launch(float LaunchSpeed);
	
};
