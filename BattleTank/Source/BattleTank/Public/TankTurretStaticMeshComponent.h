// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurretStaticMeshComponent.generated.h"

class ATank;

/**
 * 
 */
UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurretStaticMeshComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	UTankTurretStaticMeshComponent();

protected:
	virtual void BeginPlay() override;

	// Keep track of owner
	ATank* Owner;

	// The Turret's rotation degrees (yaw) per frame. This is an absolute value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float RotateSpeed;

public:
	// Rotate the Yaw of this turret
	void Rotate(float LaunchYaw);
	
	
};
