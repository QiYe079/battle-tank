// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimComponent.generated.h"

class ATank;
class UTankBarrelStaticMeshComponent;
class UTankTurretStaticMeshComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Keep track of its owner
	ATank* Owner;

	// Keep track of the Barrel
	UTankBarrelStaticMeshComponent* BarrelStaticComp;

	// Keep track of the Turret
	UTankTurretStaticMeshComponent* TurretStaticComp;

	// Keep track of the aiming angle in world space
	FVector WorldAimDirection;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Aim at target location with specified speed
	UFUNCTION(BlueprintCallable)
	void AimAt(const FVector& TargetLocation, float LaunchSpeed);

	// Set the barrel component
	void SetBarrelStaticComponent(UTankBarrelStaticMeshComponent* Barrel);

	// Set the turret component
	void SetTurretStaticComponent(UTankTurretStaticMeshComponent* Turret);

	// Get the aiming direction
	FVector GetWorldAimDirection() const;
	
};
