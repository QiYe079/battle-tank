// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAIController.generated.h"

class ATank;

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankAIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void LogMessageToConsole() const;

	// Keep track of the controlled tank by this AI
	ATank* ControlledTank;

	// Keep track of the player's tank
	ATank* PlayerTank;

public:

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
