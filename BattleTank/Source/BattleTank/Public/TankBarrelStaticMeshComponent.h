// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrelStaticMeshComponent.generated.h"

class ATank;

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta=(BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrelStaticMeshComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UTankBarrelStaticMeshComponent();

protected:
	virtual void BeginPlay() override;

	// Keep track of owner
	ATank* Owner;

	// The Barrel's elevating degree (pitch) per frame. This is an absolute value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float BarrelElevateSpeed;

	// The Barrel's minimum pitch value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float BarrelMinPitch;

	// The Barrel's maximum pitch value.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float BarrelMaxPitch;
	
public:
	// Elevate this barrel
	void Elevate(float LaunchPitch);
	
	
};
